---
title: "Juca"
date: 2022-10-03T21:42:39-03:00
draft: False
---

Juca é o primeiro cachorro da Mena, um salsicha que nasceu antes dela.

Ele já é velhinho mas toca o terror se não tiver ninguém de olho. Nasceu acostumado com a rua e não tem medo de nada mesmo que talvez devesse.

Tem energia de sobra e quando a Milena foi pra Lençóis com nossos pais ele ficou aqui comigo. Estamos competindo pra ver quem destrói a casa primeiro, por enquanto ele ta ganhando.