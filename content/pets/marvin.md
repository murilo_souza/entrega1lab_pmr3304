---
title: "Marvin"
date: 2022-10-03T21:42:26-03:00
draft: False
---

Marvin é o segundo cachorro da Milena, ele é um yorkshire e é muito carente. 

Muito novo, mais neném que a própria dona, ele é desesperado por atenção e chega a assustar as vezes.

Ele foi criado dentro de casa desde cedo, e super mimado pelos antigos donos, isso deixou ele muito mais fresco que o Juca se for comparar os dois.

As vezes ele passa mal, direto começa a tremer e cambalear, mas sempre fica bem no final.

Esse não deu sorte e acabou indo viajar junto, me pergunto o que ele estará fazendo agora...


