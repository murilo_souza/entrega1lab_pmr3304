---
title: "Patinhos"
date: 2022-10-04T14:52:37-03:00
draft: False
---

Os patinhos foram os animais mais recentes que ela teve contato

Embora eles sejam meus, ela ainda cuida bastante dos dois e adora falar sobre como um deles gosta mais dela que o outro.

Existem dois deles, a Ana e o Sabrino, são irmãos e cuidamos desde filhotes.

A Ana é uma pata que era amarela e ficou com bolinhas pretas por algum motivo desconhecido, enquanto que o sabrino é um pato preto que ganhou asas esverdeadas e cresceu muitão.

Nosso avó arranjou um espaço pros dois no galinheiro e eles se mantém lá faz um tempo já, porque aparentemente eles sujam as coisas demais.