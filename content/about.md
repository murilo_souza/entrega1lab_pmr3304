---
title: "Sobre"
date: 2022-10-03T21:23:57-03:00
draft: false
---

Nascida em 28 de dezembro, Milena Augusto Santos de Souza veio alegrar o mundo.

Hoje quase 12 anos depois, e ela continua trazendo felicidade para os corações de tanta gente.

A Milena (carinhosamente apelidade de Mena) recentemente foi viajar pra Lençóis com meus pais, e como frequentemente quando chegavamos em casa tinha alguma mensagem dela ou uma graça assim, pensei que seria divertido fazer um sitezinho pra ela mesmo que simples.

Só pra matar uma saudade e ganhar uma notinha que também não machuca ninguém.
